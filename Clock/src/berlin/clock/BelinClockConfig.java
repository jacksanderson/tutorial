package berlin.clock;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BelinClockConfig {

	public BelinClockConfig() {
////		Create a representation of the Berlin Clock for a given time (hh::mm:ss).
////		The Berlin Uhr (Clock) is a rather strange way to show the time. On the top of 
//		the clock there is a yellow lamp that blinks on/off every two seconds. 
//		The time is calculated by adding rectangular lamps.
////		The top two rows of lamps are red. These indicate the hours of a day. 
//		In the top row there are 4 red lamps. Every lamp represents 5 hours. 
//		In the lower row of red lamps every lamp represents 1 hour. 
//		So if two lamps of the first row and three of the second row are switched on that indicates 5+5+3=13h or 1 pm.
////		The two rows of lamps at the bottom count the minutes. 
//		The first of these rows has 11 lamps, the second 4. In the first row every lamp represents 5 minutes. 
//		In this first row the 3rd, 6th and 9th lamp are red and indicate the first quarter, 
//		half and last quarter of an hour. The other lamps are yellow. 
//		In the last row with 4 lamps every lamp represents 1 minute.
////		The lamps are switched on from left to right.
////		Test Cases (Y = Yellow, R = Red, O = Off)
////		Input Result 00:00:00 Y OOOO OOOO OOOOOOOOOOO OOOO
////		13:17:01 O RROO RRRO YYROOOOOOOO YYOO
////		23:59:59 O RRRR RRRO YYRYYRYYRYY YYYY
////		24:00:00 Y RRRR RRRR OOOOOOOOOOO OOOO
	}

	// On the top of the clock there is a yellow lamp that blinks on/off every two seconds.
	// yellow lamp seconds % 2
	
	// next row hours, divided by 5. remainder passed to next row i just per hour
	
	// next row 11 mins but every third goes red
	// last row just one per minute
	
	private final static char YELLOW = 'Y';
	private final static char OFF = 'O';
	private final static char RED = 'R';
	
	private volatile char[] BERLIN_SECONDS = new char[1];
	private volatile char[] BERLIN_HOURS = new char[9];
	private volatile char[] BERLIN_MINUTES = new char[16];
	
	public static void main(String[] args){
		Date formatedDate = null;
	      SimpleDateFormat ft = 
	      new SimpleDateFormat("hh:mm:ss");
	      try {
	    	  formatedDate = ft.parse("13:17:01");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      System.out.println(String.format("%1$s %2$s %3$s",seconds(formatedDate.getSeconds()), hours(formatedDate.getHours()), minutes(formatedDate.getMinutes())));
	  	
	}
	
	public static String hours(int hours){
		char[] chars = new char[9];
		int blocksOfFiveHours = hours / 5;
		int remainingHours = hours % 5;
		for(int i = 0;i < 4; i++){
			if(blocksOfFiveHours > 0){
				chars[i] = RED;
			} else {
				chars[i] = OFF;
			}
			blocksOfFiveHours--;
		}
		chars[4] = ' ';
		for(int i = 5;i < 9; i++){
			if(remainingHours > 0){
				chars[i] = RED;
			} else {
				chars[i] = OFF;
			}
			remainingHours--;
		}
		return new String(chars);
	}
	
	public static String seconds(int seconds){
		if(seconds % 2 == 0){
			return new Character(YELLOW).toString();
		} else {
			return new Character(OFF).toString();

		}
		
	}
	
	public static String minutes(int mins){
		char[] chars = new char[16];
		int blocksOfFive = mins / 5;
		int remainingHours = mins % 5;
		int trackQuartHours = 0;
		for(int i = 0;i < 11; i++){
			if(blocksOfFive > 0){
				trackQuartHours = trackQuartHours + 5;
				if(trackQuartHours % 15 == 0){
					chars[i] = YELLOW;
				} else {
					chars[i] = RED;
				}
			} else {
				chars[i] = OFF;
			}
			blocksOfFive--;
		}
		chars[11] = ' ';
		for(int i = 12;i < 16; i++){
			if(remainingHours > 0){
				chars[i] = RED;
			} else {
				chars[i] = OFF;
			}
			remainingHours--;
		}
		return new String(chars);
	}
}
